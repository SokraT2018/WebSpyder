﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Common
{
    public static class HttpStatusCodeChecker
    {
        public static HttpStatusCode GetStatusCode(string resourse)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    return httpClient
                           .GetAsync(resourse)
                           .Result
                           .StatusCode;
                }
            }
            catch(Exception)
            {
                return HttpStatusCode.BadRequest;
            }
        }

        public static async Task<HttpStatusCode> GetStatusCodeAsync(Uri url)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var responseMessage = await httpClient.GetAsync(url).ConfigureAwait(false);
                    
                    return responseMessage.StatusCode;
                }
            }
            catch
            { 
               return HttpStatusCode.BadRequest;
            }
        }

        public static bool IsStatusOK(string resource)
        {
            var httpStatusCode = GetStatusCode(resource);
            return httpStatusCode.Equals(HttpStatusCode.OK);
        }

        public static async Task<bool> IsStatusOKAsync(Uri url)
        {
            var httpStatusCode = await GetStatusCodeAsync(url).ConfigureAwait(false);
            return httpStatusCode.Equals(HttpStatusCode.OK);
        }
    }
}
