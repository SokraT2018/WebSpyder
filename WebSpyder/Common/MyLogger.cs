﻿using System.Threading.Tasks;
using System;
using System.IO;
using System.Security;

namespace Common
{
    public static class MyLogger
    {
        private const string fileExtention = ".txt";
        private const string dir = "../../../../public/log/";

        public static async Task Log(string data)
        {
            var file =  GetCurrentFileName();

            using (var sw = new StreamWriter(dir + file, true))
            {
                 var time = DateTime.Now.ToLongTimeString();
                 await sw.WriteLineAsync(time).ConfigureAwait(true);
                 await sw.WriteLineAsync(data).ConfigureAwait(true);
            }
        }

        public static async void Log(Exception ex)
        {
            if (null != ex)
            {
                await Log(ex.ToString()).ConfigureAwait(false);
            }
        }


        private static string GetCurrentFileName()
        {
            return "log_" + DateTime.Now.ToShortDateString().Replace(":", "_").Replace(".", "_") + fileExtention;
        }

    }
}


