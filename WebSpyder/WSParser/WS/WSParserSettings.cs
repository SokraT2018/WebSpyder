﻿using Parser.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.WS
{
    public class WSParserSettings : IParserSettings
    {
        public Uri PageUrl { get; }

        public Uri HostUrl { get; }

        public List<string> Selectors { get; }

        public bool NeedParseStyles { get; }

        public bool NeedFixImages { get; }

        public WSParserSettings(Uri url, List<string> selectors, bool parseStyles, bool fixImages)
        {
            PageUrl =  url;
            Selectors = selectors;
            NeedParseStyles = parseStyles;
            NeedFixImages = fixImages;
            HostUrl = GetBaseUrl(PageUrl);
        }

        public Uri GetBaseUrl(Uri pageUrl)
        {
            var url = pageUrl;

            if (null != pageUrl)
            {
                url = new Uri("https://" + pageUrl.Host);
            }
            return url;
        }

    }
}


