﻿using AngleSharp;
using AngleSharp.Dom;
using Parser.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Parser.WS
{
    public class WSParser : IParser<IElement>
    {
        private WSParserSettings ParserSettings { get; }

        public WSParser(WSParserSettings wsParserSettings)
        {
            ParserSettings = wsParserSettings;
        }

        public List<IElement> Parse(string source)
        {
            var elements = new List<IElement>();           
       
            var document = GetDocumentFromHtml(source).Result;
            
            if (null != document)
            {
                 elements = GetElemensBySelectors(document, ParserSettings.Selectors);
            }

            return elements;
        }

        public static async Task<IDocument> GetDocumentFromHtml(string html)
        {
            var config = Configuration.Default.WithXPath();
            var context = BrowsingContext.New(config);
            var document = await context.OpenAsync(res => res.Content(html)).ConfigureAwait(false);

            return document;
        }

        private static List<IElement> GetElemensBySelectors(IDocument document, List<string> selectors)
        {
            var elements = new List<IElement>();

            IHtmlCollection<IElement> parsedElements;

            foreach (var selector in selectors)
            {
                try
                {
                    parsedElements = document.QuerySelectorAll($"*[xpath>'{selector}']");

                    if(!parsedElements.Any())
                    {
                        parsedElements = document.QuerySelectorAll(selector);
                    }

                    elements.AddRange(parsedElements.ToList());
                }
                catch(Exception ex)
                {
                    Common.MyLogger.Log(ex);
                }
            }

            return elements; 
        }      
    }
}