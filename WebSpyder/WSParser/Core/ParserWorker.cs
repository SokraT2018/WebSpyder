﻿using AngleSharp.Dom;
using System;
using System.Collections.Generic;

namespace Parser.Core
{
    public class ParserWorker<T> where T : class
    {
        private IParser<T> parser { get; set; }

        private IParserSettings parserSettings { get; set; }

        private HtmlBuilder<T> htmlBuilder { get; set; }
        
        public IParser<T> Parser
        {
            get
            {
                return parser;
            }
            set
            {
                parser = value;
            }
        }

        public IParserSettings Settings
        {
            get
            {
                return parserSettings;
            }
            set
            {
                parserSettings = value;
                
            }
        }

        public event Action<object, string> OnCompleted;

        public ParserWorker(IParser<T> parser)
        {
            this.parser = parser;
        }

        public ParserWorker(IParser<T> parser, IParserSettings parserSettings) : this(parser)
        {
            this.Settings = parserSettings;
            htmlBuilder = new HtmlBuilder<T>(parserSettings);
        }

        public void Start()
        {
            Worker();
        }

        public void Start(string htmlSource)
        {
            Worker(htmlSource);
        }

        private async void Worker()
        {
            var htmlSource = await HtmlLoader.GetHtmlSource(Settings.PageUrl).ConfigureAwait(false);
            DoWork(htmlSource);
        }

        private void Worker(string htmlSource)
        {
            DoWork(htmlSource);
        }
       
        private void DoWork(string htmlSource)
        {
            var result = parser.Parse(htmlSource);
            var html = htmlBuilder.BuildHtml(result as List<IElement>, htmlSource);

            OnCompleted?.Invoke(this, html);
        }

    }
}
