﻿using System;
using System.Collections.Generic;

namespace Parser.Core
{
    public interface IParserSettings
    {        
        Uri PageUrl { get; }

        Uri HostUrl { get; }

        List<string> Selectors { get; }

        bool NeedParseStyles { get; }

        bool NeedFixImages { get; }

        Uri GetBaseUrl(Uri pageUrl);
    }
}
