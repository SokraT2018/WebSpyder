﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
 
namespace Parser.Core
{
    public static class HtmlLoader
    {
        public static async Task<string> GetHtmlSource(Uri url)
        {
            string source = String.Empty;

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(url).ConfigureAwait(false);
               
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                       source = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
            return source;
        }  
    }
}
