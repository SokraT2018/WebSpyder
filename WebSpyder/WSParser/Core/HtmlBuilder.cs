﻿using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Fixer;

namespace Parser.Core
{
    public class HtmlBuilder<T>
    {
        private IParserSettings Settings;

        private StringBuilder builder;

        private string htmlSource;
              
        //results page predefined classes for CSS
        private readonly string res_tbl = "res-tbl";
        private readonly string res_item = "res-item";
        private readonly string ws = "ws";

        public HtmlBuilder(IParserSettings settings)
        {
            Settings = settings;
        }
 
        public string BuildHtml(List<IElement> parsedElements, string htmlSource)
        {
            this.htmlSource = htmlSource;
            builder = new StringBuilder();

            builder.Append("<!DOCTYPE html>");
            builder.Append("<html>");

            if (parsedElements.Any())
            {
                BuildHead()
                .BuildBody(parsedElements);
            }
            else
            {
                builder.Append("<div style=\"font-size:24px; margin:auto;\">No results found</div>");
            }

            builder.Append("</html>");

            return builder.ToString();
        }

        private HtmlBuilder<T> BuildHead()
        {
            //head
            builder.Append("<head>");
            builder.Append("<meta charset=\"utf-8\">");
            
            IncludeCss();
           
            builder.Append("</head>");

            return this;
        }

        private HtmlBuilder<T> BuildBody(List<IElement> elements)
        {
            builder.Append("<body>");

            BuildResultTable(elements);

            builder.Append("</body>");

            return this;
        }

        private void BuildResultTable(List<IElement> elements)
        {
            if (Settings.NeedFixImages.Equals(true))
            {
                HtmlAtributesFixer.TryFixImagesSrc(elements, Settings.HostUrl);
            }

            uint i = 0;
                        
            builder.Append($"<table class=\"{ws} {res_tbl}\">");
            builder.Append("<tboby>");
        
            foreach (var el in elements)
            {
                builder.Append($"<tr class=\"{ws} {res_item}\"><td>{++i}</td><td>{el.OuterHtml}</td></tr>");
            }

            builder.Append("</tboby>");
            builder.Append("</table>");
        }

        private async void IncludeCss()
        {
            if (Settings.NeedParseStyles.Equals(true))
            {
                var document = await GetDocumentFromHtml(htmlSource).ConfigureAwait(true);

                var links = GetStylesheetsLinks(document);
                var styleTags = GetElementsByTagName(document, "style");

                foreach (var link in links)
                {
                    builder.Append(link.OuterHtml);
                }

                foreach (var tag in styleTags)
                {
                    builder.Append(tag.OuterHtml);
                }
            }

            builder.Append(GetResutsPageStyles());
        }

        public string GetResutsPageStyles()
        {
            var style = ".ws{}"
                + ".res-item{margin-bottom: 5px; background:#fff9f5;}"
                + ".res-tbl{margin:auto;}";

            return $"<style>{style}</style>";
        }

        private List<IElement> GetStylesheetsLinks(IDocument document)
        {
            var result = document.QuerySelectorAll("link[rel=\"stylesheet\"]").ToList();

            HtmlAtributesFixer.FixAtributeUrl(result, "href", Settings.HostUrl);

            return result;
        }

        private static List<IElement> GetElementsByTagName(IDocument document, string tagName)
        {
            var result = new List<IElement>();

            try
            {
                result = document.QuerySelectorAll(tagName).ToList();
            }
            catch (Exception ex)
            {
                Common.MyLogger.Log(ex);
            }

            return result;
        }

        private static async Task<IDocument> GetDocumentFromHtml(string html)
        {
            //convert string source to IDocument
            var options = new HtmlParserOptions()
            {
                IsScripting = false,
                IsEmbedded = false
            };
            var parser = new HtmlParser(options);
            IDocument document = await parser.ParseDocumentAsync(html).ConfigureAwait(false);
           
            return document;
        }

    }
}
