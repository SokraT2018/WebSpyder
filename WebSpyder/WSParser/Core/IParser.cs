﻿using AngleSharp.Html.Dom;
using System.Collections.Generic;

namespace Parser.Core
{
    public interface IParser<T> where T : class
    {
        List<T> Parse(string htmlSource);
    }
}
