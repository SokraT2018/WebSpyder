﻿using System;
using Common;

namespace Parser.Fixer
{
    public static class UrlFixer
    {
        private const string https = "https";

        public static string TryFixLink(string link, Uri baseSiteAddress)
        {
            string result = link;

            if (link != null && baseSiteAddress != null)
            {
                if (!IsLinkNotBroken(link))
                {
                    if (link[0] == '/' && link[1] != '/')
                    {
                        if (!link.Contains(baseSiteAddress.Host))
                        {
                            result = https + "://" + baseSiteAddress.Host + link;
                        }
                    }
                    else
                    if (link[0] == '/' && link[1] == '/')
                    {
                        result = https + ":" + link;
                    }
                    else
                    if (!link.Contains(https) && link[0] != '/')
                    {
                        result = https + "://" + baseSiteAddress.Host + "/" + link;
                    }
                }
            }
    
            return result;
        }

        public static string FixSrcSetLinks(string srcSet, Uri baseSiteAddress)
        {
            string fixedSet = string.Empty;

            try
            { 
                if (!string.IsNullOrEmpty(srcSet))
                {
                    var srcSetLinks = srcSet.Split(',');
                    foreach(var el in srcSetLinks)
                    {
                       var link = el.Trim();
                        fixedSet += TryFixLink(link, baseSiteAddress) + ",";
                    }
                }
            }
            catch(Exception ex)
            {
                MyLogger.Log(ex);
            }

            return fixedSet;
        }

        private static bool IsLinkNotBroken(string link)
        {
            var status = HttpStatusCodeChecker.IsStatusOK(link);
            return status;
        }
    }
}
