﻿using AngleSharp.Dom;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser.Fixer
{
    public static class HtmlAtributesFixer
    {
        public static void FixAtributeUrl(List<IElement> elements, string atributeName, Uri baseUrl)
        {
            if (null != elements && elements.Any())
            {
                foreach (var el in elements)
                {
                    var url = el.GetAttribute(atributeName);
                    url = UrlFixer.TryFixLink(url, baseUrl);
                    el.SetAttribute(atributeName, url);
                }
            }
        }

        public static void TryFixImagesSrc(List<IElement> htmlElenemts, Uri baseUrl)
        {
            const string tagName = "img";
             
            if (null == htmlElenemts)
            {
                return;
            }
         
            foreach (var el in htmlElenemts)
            {
                if (IsItImage(el))
                {
                    FixImageLink(el, baseUrl);
                }
                else if (el.OuterHtml.Contains(tagName))
                {
                    var imgTags = el.GetElementsByTagName(tagName).ToList();
                    FixImagesLinks(imgTags, baseUrl);
                }
            }
        }

        public static void FixImageLink(IElement imgTag, Uri baseUrl)
        { 
            if (null != imgTag)
            {
                var url = imgTag.GetAttribute("src");
                var srcset = imgTag.GetAttribute("srcset");

                if (!string.IsNullOrEmpty(url))
                {
                    url = Fixer.UrlFixer.TryFixLink(url, baseUrl);
                    imgTag.SetAttribute("src", url);
                }

                if (!string.IsNullOrEmpty(srcset))
                {
                    srcset = Fixer.UrlFixer.FixSrcSetLinks(srcset, baseUrl);
                    imgTag.SetAttribute("srcset", srcset);
                }
            }
        }

        public static void FixImagesLinks(List<IElement> imgTags, Uri baseUrl)
        {
            if (null != imgTags)
            {
                foreach (var img in imgTags)
                {
                    FixImageLink(img, baseUrl);
                }
            }
        }

        private static bool IsItImage(IElement element)
        {   
            //need to use uppercased html tags names only
            const string imgTagName = "IMG";

            return element.TagName.Equals(imgTagName);
        }
    }
}
