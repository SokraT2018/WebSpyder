﻿namespace WebSpyder.Controls.ParserControls
{
    partial class ParserControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParserControl));
            this.tab1Container = new System.Windows.Forms.TableLayoutPanel();
            this.parserContainer = new System.Windows.Forms.Panel();
            this.parserTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.selectorsContainer_pnl = new System.Windows.Forms.Panel();
            this.selectorsInfo_lbl = new System.Windows.Forms.Label();
            this.selectors_TxtBx = new System.Windows.Forms.RichTextBox();
            this.selectors_lbl = new System.Windows.Forms.Label();
            this.urlPanel = new System.Windows.Forms.Panel();
            this.url_lbl = new System.Windows.Forms.Label();
            this.urlStatus_bttn = new System.Windows.Forms.Label();
            this.urlField_TxtBx = new System.Windows.Forms.TextBox();
            this.parserSettings_pnl = new System.Windows.Forms.Panel();
            this.fixImgSrc_chckBx = new System.Windows.Forms.CheckBox();
            this.parseStyle_chckBx = new System.Windows.Forms.CheckBox();
            this.getBrowserSource_chckBx = new System.Windows.Forms.CheckBox();
            this.parse_bttn = new System.Windows.Forms.Button();
            this.renderContainer_pnl = new System.Windows.Forms.Panel();
            this.statusToltip = new System.Windows.Forms.ToolTip(this.components);
            this.selectorsInfo_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.selectorsFieldEmpty_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.InvalidUrl_tlTp = new System.Windows.Forms.ToolTip(this.components);
            this.tab1Container.SuspendLayout();
            this.parserContainer.SuspendLayout();
            this.parserTableLayoutPanel.SuspendLayout();
            this.selectorsContainer_pnl.SuspendLayout();
            this.urlPanel.SuspendLayout();
            this.parserSettings_pnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1Container
            // 
            this.tab1Container.ColumnCount = 2;
            this.tab1Container.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tab1Container.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tab1Container.Controls.Add(this.parserContainer, 0, 0);
            this.tab1Container.Controls.Add(this.renderContainer_pnl, 1, 0);
            this.tab1Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab1Container.Location = new System.Drawing.Point(0, 0);
            this.tab1Container.Margin = new System.Windows.Forms.Padding(0);
            this.tab1Container.Name = "tab1Container";
            this.tab1Container.RowCount = 1;
            this.tab1Container.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tab1Container.Size = new System.Drawing.Size(924, 623);
            this.tab1Container.TabIndex = 3;
            // 
            // parserContainer
            // 
            this.parserContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.parserContainer.Controls.Add(this.parserTableLayoutPanel);
            this.parserContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parserContainer.Location = new System.Drawing.Point(0, 0);
            this.parserContainer.Margin = new System.Windows.Forms.Padding(0);
            this.parserContainer.Name = "parserContainer";
            this.parserContainer.Size = new System.Drawing.Size(415, 623);
            this.parserContainer.TabIndex = 5;
            // 
            // parserTableLayoutPanel
            // 
            this.parserTableLayoutPanel.BackColor = System.Drawing.Color.DarkGray;
            this.parserTableLayoutPanel.ColumnCount = 1;
            this.parserTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.parserTableLayoutPanel.Controls.Add(this.selectorsContainer_pnl, 0, 5);
            this.parserTableLayoutPanel.Controls.Add(this.urlPanel, 0, 3);
            this.parserTableLayoutPanel.Controls.Add(this.parserSettings_pnl, 0, 4);
            this.parserTableLayoutPanel.Controls.Add(this.parse_bttn, 0, 7);
            this.parserTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parserTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.parserTableLayoutPanel.Name = "parserTableLayoutPanel";
            this.parserTableLayoutPanel.RowCount = 9;
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.parserTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.parserTableLayoutPanel.Size = new System.Drawing.Size(415, 623);
            this.parserTableLayoutPanel.TabIndex = 13;
            this.parserTableLayoutPanel.TabStop = true;
            // 
            // selectorsContainer_pnl
            // 
            this.selectorsContainer_pnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectorsContainer_pnl.Controls.Add(this.selectorsInfo_lbl);
            this.selectorsContainer_pnl.Controls.Add(this.selectors_TxtBx);
            this.selectorsContainer_pnl.Controls.Add(this.selectors_lbl);
            this.selectorsContainer_pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectorsContainer_pnl.Location = new System.Drawing.Point(0, 289);
            this.selectorsContainer_pnl.Margin = new System.Windows.Forms.Padding(0);
            this.selectorsContainer_pnl.Name = "selectorsContainer_pnl";
            this.selectorsContainer_pnl.Size = new System.Drawing.Size(415, 137);
            this.selectorsContainer_pnl.TabIndex = 16;
            // 
            // selectorsInfo_lbl
            // 
            this.selectorsInfo_lbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectorsInfo_lbl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectorsInfo_lbl.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectorsInfo_lbl.Image = ((System.Drawing.Image)(resources.GetObject("selectorsInfo_lbl.Image")));
            this.selectorsInfo_lbl.Location = new System.Drawing.Point(10, 9);
            this.selectorsInfo_lbl.Name = "selectorsInfo_lbl";
            this.selectorsInfo_lbl.Size = new System.Drawing.Size(18, 18);
            this.selectorsInfo_lbl.TabIndex = 13;
            // 
            // selectors_TxtBx
            // 
            this.selectors_TxtBx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectors_TxtBx.BackColor = System.Drawing.Color.White;
            this.selectors_TxtBx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectors_TxtBx.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectors_TxtBx.ForeColor = System.Drawing.Color.Black;
            this.selectors_TxtBx.Location = new System.Drawing.Point(10, 30);
            this.selectors_TxtBx.Margin = new System.Windows.Forms.Padding(10);
            this.selectors_TxtBx.Name = "selectors_TxtBx";
            this.selectors_TxtBx.Size = new System.Drawing.Size(393, 90);
            this.selectors_TxtBx.TabIndex = 12;
            this.selectors_TxtBx.Text = "";
            // 
            // selectors_lbl
            // 
            this.selectors_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.selectors_lbl.AutoSize = true;
            this.selectors_lbl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectors_lbl.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectors_lbl.Location = new System.Drawing.Point(30, 8);
            this.selectors_lbl.Margin = new System.Windows.Forms.Padding(3);
            this.selectors_lbl.Name = "selectors_lbl";
            this.selectors_lbl.Size = new System.Drawing.Size(90, 19);
            this.selectors_lbl.TabIndex = 0;
            this.selectors_lbl.Text = "Selectors:";
            // 
            // urlPanel
            // 
            this.urlPanel.Controls.Add(this.url_lbl);
            this.urlPanel.Controls.Add(this.urlStatus_bttn);
            this.urlPanel.Controls.Add(this.urlField_TxtBx);
            this.urlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.urlPanel.Location = new System.Drawing.Point(3, 74);
            this.urlPanel.Name = "urlPanel";
            this.urlPanel.Size = new System.Drawing.Size(409, 79);
            this.urlPanel.TabIndex = 17;
            // 
            // url_lbl
            // 
            this.url_lbl.AutoSize = true;
            this.url_lbl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.url_lbl.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.url_lbl.Location = new System.Drawing.Point(40, 10);
            this.url_lbl.Name = "url_lbl";
            this.url_lbl.Size = new System.Drawing.Size(39, 19);
            this.url_lbl.TabIndex = 14;
            this.url_lbl.Text = "Url:";
            // 
            // urlStatus_bttn
            // 
            this.urlStatus_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.urlStatus_bttn.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urlStatus_bttn.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.urlStatus_bttn.Image = ((System.Drawing.Image)(resources.GetObject("urlStatus_bttn.Image")));
            this.urlStatus_bttn.Location = new System.Drawing.Point(7, 32);
            this.urlStatus_bttn.Name = "urlStatus_bttn";
            this.urlStatus_bttn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.urlStatus_bttn.Size = new System.Drawing.Size(27, 27);
            this.urlStatus_bttn.TabIndex = 13;
            // 
            // urlField_TxtBx
            // 
            this.urlField_TxtBx.AllowDrop = true;
            this.urlField_TxtBx.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.urlField_TxtBx.Location = new System.Drawing.Point(35, 32);
            this.urlField_TxtBx.Margin = new System.Windows.Forms.Padding(0);
            this.urlField_TxtBx.Name = "urlField_TxtBx";
            this.urlField_TxtBx.Size = new System.Drawing.Size(352, 27);
            this.urlField_TxtBx.TabIndex = 12;
            this.urlField_TxtBx.Text = "https://kherson.itstep.org/";
            this.InvalidUrl_tlTp.SetToolTip(this.urlField_TxtBx, "123");
            this.urlField_TxtBx.Leave += new System.EventHandler(this.CheckUrlHttpStatus);
            // 
            // parserSettings_pnl
            // 
            this.parserSettings_pnl.Controls.Add(this.fixImgSrc_chckBx);
            this.parserSettings_pnl.Controls.Add(this.parseStyle_chckBx);
            this.parserSettings_pnl.Controls.Add(this.getBrowserSource_chckBx);
            this.parserSettings_pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parserSettings_pnl.Location = new System.Drawing.Point(3, 159);
            this.parserSettings_pnl.Name = "parserSettings_pnl";
            this.parserSettings_pnl.Size = new System.Drawing.Size(409, 127);
            this.parserSettings_pnl.TabIndex = 20;
            // 
            // fixImgSrc_chckBx
            // 
            this.fixImgSrc_chckBx.AutoSize = true;
            this.fixImgSrc_chckBx.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fixImgSrc_chckBx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixImgSrc_chckBx.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fixImgSrc_chckBx.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.fixImgSrc_chckBx.Location = new System.Drawing.Point(81, 75);
            this.fixImgSrc_chckBx.Name = "fixImgSrc_chckBx";
            this.fixImgSrc_chckBx.Size = new System.Drawing.Size(133, 22);
            this.fixImgSrc_chckBx.TabIndex = 20;
            this.fixImgSrc_chckBx.Text = "Fix Url of images";
            this.fixImgSrc_chckBx.UseVisualStyleBackColor = true;
            // 
            // parseStyle_chckBx
            // 
            this.parseStyle_chckBx.AutoSize = true;
            this.parseStyle_chckBx.Cursor = System.Windows.Forms.Cursors.Hand;
            this.parseStyle_chckBx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.parseStyle_chckBx.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.parseStyle_chckBx.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.parseStyle_chckBx.Location = new System.Drawing.Point(81, 47);
            this.parseStyle_chckBx.Name = "parseStyle_chckBx";
            this.parseStyle_chckBx.Size = new System.Drawing.Size(222, 22);
            this.parseStyle_chckBx.TabIndex = 0;
            this.parseStyle_chckBx.Text = "Use CSS from the target page";
            this.parseStyle_chckBx.UseVisualStyleBackColor = true;
            // 
            // getBrowserSource_chckBx
            // 
            this.getBrowserSource_chckBx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.getBrowserSource_chckBx.AutoSize = true;
            this.getBrowserSource_chckBx.Cursor = System.Windows.Forms.Cursors.Hand;
            this.getBrowserSource_chckBx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.getBrowserSource_chckBx.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.getBrowserSource_chckBx.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.getBrowserSource_chckBx.Location = new System.Drawing.Point(81, 19);
            this.getBrowserSource_chckBx.Name = "getBrowserSource_chckBx";
            this.getBrowserSource_chckBx.Size = new System.Drawing.Size(219, 22);
            this.getBrowserSource_chckBx.TabIndex = 19;
            this.getBrowserSource_chckBx.Text = "Parse  current  browser page ";
            this.getBrowserSource_chckBx.UseVisualStyleBackColor = true;
            this.getBrowserSource_chckBx.CheckedChanged += new System.EventHandler(this.GetBrowserSource_chckBx_CheckedChanged);
            // 
            // parse_bttn
            // 
            this.parse_bttn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.parse_bttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.parse_bttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.parse_bttn.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.parse_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.parse_bttn.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.parse_bttn.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.parse_bttn.Location = new System.Drawing.Point(64, 454);
            this.parse_bttn.Name = "parse_bttn";
            this.parse_bttn.Size = new System.Drawing.Size(286, 41);
            this.parse_bttn.TabIndex = 11;
            this.parse_bttn.Text = "Parse";
            this.parse_bttn.UseVisualStyleBackColor = false;
            this.parse_bttn.Click += new System.EventHandler(this.Parse_Btn_Click);
            // 
            // renderContainer_pnl
            // 
            this.renderContainer_pnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.renderContainer_pnl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.renderContainer_pnl.Location = new System.Drawing.Point(415, 0);
            this.renderContainer_pnl.Margin = new System.Windows.Forms.Padding(0);
            this.renderContainer_pnl.MinimumSize = new System.Drawing.Size(20, 20);
            this.renderContainer_pnl.Name = "renderContainer_pnl";
            this.renderContainer_pnl.Size = new System.Drawing.Size(509, 623);
            this.renderContainer_pnl.TabIndex = 4;
            this.renderContainer_pnl.Leave += new System.EventHandler(this.CheckUrlHttpStatus);
            // 
            // selectorsInfo_tooltip
            // 
            this.selectorsInfo_tooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // ParserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tab1Container);
            this.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ParserControl";
            this.Size = new System.Drawing.Size(924, 623);
            this.Load += new System.EventHandler(this.ParserTab_Load);
            this.SizeChanged += new System.EventHandler(this.Size_Changed);
            this.tab1Container.ResumeLayout(false);
            this.parserContainer.ResumeLayout(false);
            this.parserTableLayoutPanel.ResumeLayout(false);
            this.selectorsContainer_pnl.ResumeLayout(false);
            this.selectorsContainer_pnl.PerformLayout();
            this.urlPanel.ResumeLayout(false);
            this.urlPanel.PerformLayout();
            this.parserSettings_pnl.ResumeLayout(false);
            this.parserSettings_pnl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tab1Container;
        private System.Windows.Forms.Panel parserContainer;
        private System.Windows.Forms.Button parse_bttn;
        private System.Windows.Forms.TableLayoutPanel parserTableLayoutPanel;
        private System.Windows.Forms.ToolTip statusToltip;
        private System.Windows.Forms.Panel selectorsContainer_pnl;
        private System.Windows.Forms.Label selectors_lbl;
        private System.Windows.Forms.Panel urlPanel;
        private System.Windows.Forms.Label urlStatus_bttn;
        private System.Windows.Forms.TextBox urlField_TxtBx;
        private System.Windows.Forms.Label url_lbl;
        private System.Windows.Forms.Label selectorsInfo_lbl;
        private System.Windows.Forms.ToolTip selectorsInfo_tooltip;
        private System.Windows.Forms.ToolTip selectorsFieldEmpty_tooltip;
        private System.Windows.Forms.Panel parserSettings_pnl;
        private System.Windows.Forms.CheckBox getBrowserSource_chckBx;
        private System.Windows.Forms.CheckBox fixImgSrc_chckBx;
        private System.Windows.Forms.CheckBox parseStyle_chckBx;
        private System.Windows.Forms.Panel renderContainer_pnl;
        private System.Windows.Forms.ToolTip InvalidUrl_tlTp;
        private System.Windows.Forms.RichTextBox selectors_TxtBx;
    }
}
