﻿using AngleSharp.Dom;
using Parser.WS;
using Parser.Core;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WebSpyder.Controls.RendererControls;
using CefSharp.WinForms;
using CefSharp;
using System.Net;

namespace WebSpyder.Controls.ParserControls
{
    public partial class ParserControl : UserControl
    {
        private ParserWorker<IElement> parser;

        private static WSParserSettings parserSettings;

        private const char selectorsSeparator = ';';
       
        private static HtmlRenderer renderer;

        private ChromiumWebBrowser browser;

        private delegate void InvokableDataSetter_Dlgt(); 

        public ParserControl()
        {
            InitializeComponent();
            InitializeRenderer();
            SetStyle();
        }

        public void ParserOnCompleted(object sender, string html)
        {
            HtmlRenderer.Render(html);
        }

        public void SetBrowser(ChromiumWebBrowser browser)
        {
            this.browser = browser;
        }

        private void InitializeRenderer()
        {
            renderer = new HtmlRenderer();
            renderer.Dock = DockStyle.Fill;

            renderContainer_pnl.Dock = DockStyle.Fill;
            renderContainer_pnl.Controls.Add(renderer);
        }

        private void ParserTab_Load(object sender, EventArgs e)
        {
            this.statusToltip.SetToolTip(urlStatus_bttn, "Status: Unknown");
            this.selectorsInfo_tooltip.SetToolTip(selectorsInfo_lbl, "Available using of CSS selectors and XPath separeted by semicolon");
        }

        private void Size_Changed(object sender, EventArgs e)
        {
            renderContainer_pnl.Size =  new System.Drawing.Size(this.Size.Width - Visual.Sizes.ParserControlWidth, this.Size.Height);
        }

        private void GetBrowserSource_chckBx_CheckedChanged(object sender, EventArgs e)
        {
            if (this.getBrowserSource_chckBx.Checked)
            {
                if (InvokeRequired)
                {
                    InvokableDataSetter_Dlgt changeUrl = new InvokableDataSetter_Dlgt
                    (
                        () => { this.urlField_TxtBx.Text = browser.Address; }
                    );
                    
                    Invoke(changeUrl);
                }
                else
                {
                    this.urlField_TxtBx.Text = browser.Address;
                }
            }
        }

        private async void CheckUrlHttpStatus(object sender, EventArgs e)
        {
            try
            { 
                var Url = GetUrl();
                var status = await HttpStatusCodeChecker.GetStatusCodeAsync(Url).ConfigureAwait(false);
                SetUrlStatusInfo(status);
            }
            catch(UriFormatException)
            {                   
                ShowInvalidUrlMessage();
            }
        }
    
        private bool SetUrlStatusInfo(HttpStatusCode status)
        {
            bool isResponeCorrect = true;
            var statusCode = (int)status;
           
            if(statusCode >= (int)HttpStatusCode.BadRequest)
            {
                urlStatus_bttn.BackColor = System.Drawing.Color.OrangeRed;
                isResponeCorrect = false;
            }
            else if (statusCode >= (int)HttpStatusCode.MultipleChoices)
            {
                urlStatus_bttn.BackColor = System.Drawing.Color.Yellow;
            }
            else if (statusCode >= (int)HttpStatusCode.Continue)
            {
                urlStatus_bttn.BackColor = System.Drawing.Color.ForestGreen;
            }
            else
            {
                urlStatus_bttn.BackColor = System.Drawing.Color.DarkSlateGray;
                statusToltip.SetToolTip(urlStatus_bttn, "Status: Unknown");
            }
            statusToltip.SetToolTip(urlStatus_bttn, $"Status: {statusCode} {status.ToString()}");
            
            return isResponeCorrect;
        }

        private void Parse_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                var Url = GetUrl().ToString();
                var status =  HttpStatusCodeChecker.GetStatusCode(Url);
                SetUrlStatusInfo(status);
               
                if (string.IsNullOrEmpty(selectors_TxtBx.Text))
                {
                    var message = "You have no enter selectors values";
                    MessageBox.Show(message, "Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {  
                    RunParser();
                }
            }
            catch(UriFormatException _)
            {
                ShowInvalidUrlMessage();
            }
        }

        private void RunParser()
        {
            try
            { 
                InitializeParser();

                if (getBrowserSource_chckBx.Checked)
                {
                    var source = browser.GetSourceAsync();
                    parser.Start(source.Result);
                }
                else
                {
                    parser.Start();
                }
            }
            catch(Exception ex)
            {
                Logger.Log.Error("New unhandled exception", ex);
            }
        }

        private void InitializeParser()
        {
            InitializeParserSettings();
            parser = new ParserWorker<IElement>
            (
                new WSParser(parserSettings),
                parserSettings
            );

            parser.OnCompleted += ParserOnCompleted;
        }

        private void InitializeParserSettings()
        {
            try
            {
                 parserSettings = new WSParserSettings(GetUrl(), GetSelectors(), parseStyle_chckBx.Checked, fixImgSrc_chckBx.Checked);
            }
            catch (ArgumentNullException ex)
            {
                Logger.Log.Error("Argument Null Exception", ex);
            }
            catch(UriFormatException)
            {
                ShowInvalidUrlMessage();
            }
        }

        private List<string> GetSelectors()
        {
            var selectorsList = new List<string>();
            var selectorsText = selectors_TxtBx.Text;

            if (selectorsText.Contains(selectorsSeparator))
            {
                var selector = String.Empty;
                //if more than one selectors which separeted by selectorsSeparator
                foreach (var item in selectorsText)
                {
                    if (item.Equals(selectorsSeparator))
                    {
                        selectorsList.Add(selector.Trim());
                        selector = String.Empty;
                    }
                    else
                    {
                        selector += item;
                    }
                }
            }
            else
            {
               selectorsList.Add(selectorsText.Trim());
            }

            return selectorsList;
        }

        private Uri GetUrl()
        {
            var url = new Uri(urlField_TxtBx.Text);
            return url;
        }

        private void ShowInvalidUrlMessage()
        {
            if (getBrowserSource_chckBx.Checked)
            {
                MessageBox.Show("Browser url invalid or empty", "Check browser url", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                MessageBox.Show("Url invalid or empty", "Check url", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
            urlStatus_bttn.BackColor = System.Drawing.Color.OrangeRed;
            statusToltip.SetToolTip(urlStatus_bttn, "Incorrect Url");
        }

        private void SetStyle()
        {
            parse_bttn.BackColor = Visual.Colors.ButtonAccent;
            urlField_TxtBx.BackColor = Visual.Colors.InputField;
            selectors_TxtBx.BackColor = Visual.Colors.InputField;
            parserTableLayoutPanel.BackColor = Visual.Colors.Background;
        }
    }
}
