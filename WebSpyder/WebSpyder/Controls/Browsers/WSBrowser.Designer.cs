﻿namespace WebSpyder.Controls.Browsers
{
    partial class WSBrowser
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSBrowser));
            this.navBarContainer = new System.Windows.Forms.Panel();
            this.container_pnl = new System.Windows.Forms.Panel();
            this.devTools_button = new System.Windows.Forms.Button();
            this.search_bttn = new System.Windows.Forms.Button();
            this.reload_bttn = new System.Windows.Forms.Button();
            this.forward_bttn = new System.Windows.Forms.Button();
            this.back_bttn = new System.Windows.Forms.Button();
            this.addressBar_pnl = new System.Windows.Forms.ContainerControl();
            this.addresField_txtBx = new System.Windows.Forms.TextBox();
            this.go_bttn = new System.Windows.Forms.Button();
            this.browserTabContainer = new System.Windows.Forms.TableLayoutPanel();
            this.browserPage_spltCntnr = new System.Windows.Forms.SplitContainer();
            this.webPageContainer_pnl = new System.Windows.Forms.Panel();
            this.navBarContainer.SuspendLayout();
            this.container_pnl.SuspendLayout();
            this.addressBar_pnl.SuspendLayout();
            this.browserTabContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.browserPage_spltCntnr)).BeginInit();
            this.browserPage_spltCntnr.Panel1.SuspendLayout();
            this.browserPage_spltCntnr.SuspendLayout();
            this.SuspendLayout();
            // 
            // navBarContainer
            // 
            this.navBarContainer.AutoSize = true;
            this.navBarContainer.Controls.Add(this.container_pnl);
            this.navBarContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarContainer.Location = new System.Drawing.Point(0, 0);
            this.navBarContainer.Margin = new System.Windows.Forms.Padding(0);
            this.navBarContainer.Name = "navBarContainer";
            this.navBarContainer.Size = new System.Drawing.Size(807, 30);
            this.navBarContainer.TabIndex = 0;
            // 
            // container_pnl
            // 
            this.container_pnl.AutoSize = true;
            this.container_pnl.Controls.Add(this.devTools_button);
            this.container_pnl.Controls.Add(this.search_bttn);
            this.container_pnl.Controls.Add(this.reload_bttn);
            this.container_pnl.Controls.Add(this.forward_bttn);
            this.container_pnl.Controls.Add(this.back_bttn);
            this.container_pnl.Controls.Add(this.addressBar_pnl);
            this.container_pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.container_pnl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.container_pnl.Location = new System.Drawing.Point(0, 0);
            this.container_pnl.Margin = new System.Windows.Forms.Padding(0);
            this.container_pnl.Name = "container_pnl";
            this.container_pnl.Size = new System.Drawing.Size(807, 30);
            this.container_pnl.TabIndex = 1;
            // 
            // devTools_button
            // 
            this.devTools_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(0)))));
            this.devTools_button.FlatAppearance.BorderSize = 0;
            this.devTools_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.devTools_button.Image = ((System.Drawing.Image)(resources.GetObject("devTools_button.Image")));
            this.devTools_button.Location = new System.Drawing.Point(756, 2);
            this.devTools_button.Margin = new System.Windows.Forms.Padding(0);
            this.devTools_button.Name = "devTools_button";
            this.devTools_button.Size = new System.Drawing.Size(44, 24);
            this.devTools_button.TabIndex = 8;
            this.devTools_button.UseVisualStyleBackColor = false;
            this.devTools_button.Click += new System.EventHandler(this.DevTools_bttn_Click);
            // 
            // search_bttn
            // 
            this.search_bttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(0)))));
            this.search_bttn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("search_bttn.BackgroundImage")));
            this.search_bttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.search_bttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.search_bttn.FlatAppearance.BorderSize = 0;
            this.search_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.search_bttn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.search_bttn.Location = new System.Drawing.Point(697, 2);
            this.search_bttn.Margin = new System.Windows.Forms.Padding(3, 3, 5, 3);
            this.search_bttn.Name = "search_bttn";
            this.search_bttn.Size = new System.Drawing.Size(50, 24);
            this.search_bttn.TabIndex = 7;
            this.search_bttn.UseVisualStyleBackColor = true;
            this.search_bttn.Click += new System.EventHandler(this.Search_bttn_Click);
            // 
            // reload_bttn
            // 
            this.reload_bttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(0)))));
            this.reload_bttn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reload_bttn.BackgroundImage")));
            this.reload_bttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.reload_bttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reload_bttn.FlatAppearance.BorderSize = 0;
            this.reload_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reload_bttn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reload_bttn.Location = new System.Drawing.Point(97, 3);
            this.reload_bttn.Name = "reload_bttn";
            this.reload_bttn.Size = new System.Drawing.Size(45, 23);
            this.reload_bttn.TabIndex = 6;
            this.reload_bttn.Tag = "";
            this.reload_bttn.UseVisualStyleBackColor = true;
            this.reload_bttn.Click += new System.EventHandler(this.Reload_bttn_Click);
            // 
            // forward_bttn
            // 
            this.forward_bttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(0)))));
            this.forward_bttn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("forward_bttn.BackgroundImage")));
            this.forward_bttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.forward_bttn.FlatAppearance.BorderSize = 0;
            this.forward_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.forward_bttn.Location = new System.Drawing.Point(56, 3);
            this.forward_bttn.Name = "forward_bttn";
            this.forward_bttn.Size = new System.Drawing.Size(35, 23);
            this.forward_bttn.TabIndex = 5;
            this.forward_bttn.UseVisualStyleBackColor = true;
            this.forward_bttn.Click += new System.EventHandler(this.Forward_bttn_Click);
            // 
            // back_bttn
            // 
            this.back_bttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(0)))));
            this.back_bttn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("back_bttn.BackgroundImage")));
            this.back_bttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.back_bttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_bttn.FlatAppearance.BorderSize = 0;
            this.back_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back_bttn.Location = new System.Drawing.Point(16, 3);
            this.back_bttn.Name = "back_bttn";
            this.back_bttn.Size = new System.Drawing.Size(34, 23);
            this.back_bttn.TabIndex = 4;
            this.back_bttn.UseVisualStyleBackColor = true;
            this.back_bttn.Click += new System.EventHandler(this.Back_bttn_Click);
            // 
            // addressBar_pnl
            // 
            this.addressBar_pnl.AllowDrop = true;
            this.addressBar_pnl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.addressBar_pnl.Controls.Add(this.addresField_txtBx);
            this.addressBar_pnl.Controls.Add(this.go_bttn);
            this.addressBar_pnl.Location = new System.Drawing.Point(210, 3);
            this.addressBar_pnl.Margin = new System.Windows.Forms.Padding(0);
            this.addressBar_pnl.MaximumSize = new System.Drawing.Size(800, 24);
            this.addressBar_pnl.MinimumSize = new System.Drawing.Size(150, 24);
            this.addressBar_pnl.Name = "addressBar_pnl";
            this.addressBar_pnl.Size = new System.Drawing.Size(400, 24);
            this.addressBar_pnl.TabIndex = 0;
            // 
            // addresField_txtBx
            // 
            this.addresField_txtBx.AllowDrop = true;
            this.addresField_txtBx.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.addresField_txtBx.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.addresField_txtBx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addresField_txtBx.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.addresField_txtBx.Location = new System.Drawing.Point(0, 3);
            this.addresField_txtBx.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.addresField_txtBx.Name = "addresField_txtBx";
            this.addresField_txtBx.Size = new System.Drawing.Size(335, 20);
            this.addresField_txtBx.TabIndex = 0;
            this.addresField_txtBx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddresField_txtBx_KeyDown);
            // 
            // go_bttn
            // 
            this.go_bttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.go_bttn.BackColor = System.Drawing.Color.SeaGreen;
            this.go_bttn.FlatAppearance.BorderSize = 0;
            this.go_bttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.go_bttn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.go_bttn.ForeColor = System.Drawing.Color.White;
            this.go_bttn.Location = new System.Drawing.Point(335, 3);
            this.go_bttn.Margin = new System.Windows.Forms.Padding(0);
            this.go_bttn.Name = "go_bttn";
            this.go_bttn.Size = new System.Drawing.Size(58, 15);
            this.go_bttn.TabIndex = 0;
            this.go_bttn.Text = "GO";
            this.go_bttn.UseVisualStyleBackColor = false;
            this.go_bttn.Click += new System.EventHandler(this.Go_bttn_Click);
            // 
            // browserTabContainer
            // 
            this.browserTabContainer.ColumnCount = 1;
            this.browserTabContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.browserTabContainer.Controls.Add(this.navBarContainer, 0, 0);
            this.browserTabContainer.Controls.Add(this.browserPage_spltCntnr, 0, 1);
            this.browserTabContainer.Location = new System.Drawing.Point(0, 0);
            this.browserTabContainer.Margin = new System.Windows.Forms.Padding(0);
            this.browserTabContainer.Name = "browserTabContainer";
            this.browserTabContainer.RowCount = 2;
            this.browserTabContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.browserTabContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.browserTabContainer.Size = new System.Drawing.Size(800, 497);
            this.browserTabContainer.TabIndex = 2;
            // 
            // browserPage_spltCntnr
            // 
            this.browserPage_spltCntnr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserPage_spltCntnr.Location = new System.Drawing.Point(0, 30);
            this.browserPage_spltCntnr.Margin = new System.Windows.Forms.Padding(0);
            this.browserPage_spltCntnr.Name = "browserPage_spltCntnr";
            // 
            // browserPage_spltCntnr.Panel1
            // 
            this.browserPage_spltCntnr.Panel1.Controls.Add(this.webPageContainer_pnl);
            // 
            // browserPage_spltCntnr.Panel2
            // 
            this.browserPage_spltCntnr.Panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.browserPage_spltCntnr.Panel2MinSize = 0;
            this.browserPage_spltCntnr.Size = new System.Drawing.Size(807, 467);
            this.browserPage_spltCntnr.SplitterDistance = 514;
            this.browserPage_spltCntnr.TabIndex = 1;
            // 
            // webPageContainer_pnl
            // 
            this.webPageContainer_pnl.AutoSize = true;
            this.webPageContainer_pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webPageContainer_pnl.Location = new System.Drawing.Point(0, 0);
            this.webPageContainer_pnl.Margin = new System.Windows.Forms.Padding(0);
            this.webPageContainer_pnl.Name = "webPageContainer_pnl";
            this.webPageContainer_pnl.Size = new System.Drawing.Size(514, 467);
            this.webPageContainer_pnl.TabIndex = 2;
            // 
            // WSBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.browserTabContainer);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "WSBrowser";
            this.Size = new System.Drawing.Size(813, 508);
            this.Load += new System.EventHandler(this.WebBrowser_Load);
            this.SizeChanged += new System.EventHandler(this.Size_Changed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.WSBrowser_KeyDown);
            this.navBarContainer.ResumeLayout(false);
            this.navBarContainer.PerformLayout();
            this.container_pnl.ResumeLayout(false);
            this.addressBar_pnl.ResumeLayout(false);
            this.addressBar_pnl.PerformLayout();
            this.browserTabContainer.ResumeLayout(false);
            this.browserTabContainer.PerformLayout();
            this.browserPage_spltCntnr.Panel1.ResumeLayout(false);
            this.browserPage_spltCntnr.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.browserPage_spltCntnr)).EndInit();
            this.browserPage_spltCntnr.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        
        #endregion

        public System.Windows.Forms.Panel navBarContainer;
        private System.Windows.Forms.TableLayoutPanel browserTabContainer;
        private System.Windows.Forms.SplitContainer browserPage_spltCntnr;
        public System.Windows.Forms.Panel webPageContainer_pnl;
        private System.Windows.Forms.Panel container_pnl;
        private System.Windows.Forms.Button devTools_button;
        private System.Windows.Forms.Button search_bttn;
        private System.Windows.Forms.Button reload_bttn;
        private System.Windows.Forms.Button forward_bttn;
        private System.Windows.Forms.Button back_bttn;
        private System.Windows.Forms.ContainerControl addressBar_pnl;
        public System.Windows.Forms.TextBox addresField_txtBx;
        private System.Windows.Forms.Button go_bttn;

    }
}

