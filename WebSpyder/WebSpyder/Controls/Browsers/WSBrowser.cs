﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Windows.Forms;

namespace WebSpyder.Controls.Browsers
{
    public partial class WSBrowser : UserControl
    {
        delegate void SetData_dlgt();

        public ChromiumWebBrowser ChromiumBrowser { get; private set; }

        private bool isDevToolVisible = false;
        private const string googleStartPage = "https://www.google.com";

        public WSBrowser()
        {
            InitializeComponent();
            SetStyle();
            InitializeChromium();
            this.browserPage_spltCntnr.SplitterDistance = this.Size.Width;
            this.browserPage_spltCntnr.Panel2.Hide();
        }

        private void WebBrowser_Load(object sender, EventArgs e)
        {
            ChromiumBrowser.Load(googleStartPage);
        }

        private void InitializeChromium()
        {
            //Create a WSBrowser component
            ChromiumBrowser = new ChromiumWebBrowser(String.Empty);

            //Add the WSBrowser to the form
            this.webPageContainer_pnl.Controls.Add(ChromiumBrowser);
            this.ChromiumBrowser.AddressChanged += Browser_AddressChanged;
            this.ChromiumBrowser.LoadError += LoadError;
        }

        private void LoadError(object sender, LoadErrorEventArgs e)
        {
             Logger.Log.Error(e.ToString());
        }

        public void OpenUrl(string address)
        {
            this.ChromiumBrowser.Load(address);
        }

        public void LoadHtml(string html)
        {
            this.ChromiumBrowser.LoadHtml(html);
        }

        private string GetSiteAddress()
        {
            string url = string.Empty;
            try
            {
                url = this.addresField_txtBx.Text;
            }
            catch (Exception ex)
            {
                Logger.Log.Error("New unhandled exception",ex);
            }
            return url;
        }

        private void Size_Changed(object sender, EventArgs e)
        {
            browserTabContainer.Size = this.Size;
            navBarContainer.Size = new System.Drawing.Size(Size.Width, Visual.Sizes.NavigationPanelHeight);
            navBarContainer.Dock = DockStyle.Fill;
            browserPage_spltCntnr.Size = new System.Drawing.Size(Size.Width, Size.Height - Visual.Sizes.NavigationPanelHeight);
            browserPage_spltCntnr.Dock = DockStyle.Fill;
        }

        private void Browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {

            if (InvokeRequired)
            {
                SetData_dlgt changeUrl = new SetData_dlgt
                (
                    () => { this.addresField_txtBx.Text = e.Address; }
                );

                Invoke(changeUrl);
            }
            else
            {
                this.addresField_txtBx.Text = e.Address;
            }
        }

        private void Forward_bttn_Click(object sender, EventArgs e)
        {
            if (ChromiumBrowser.CanGoForward)
                ChromiumBrowser.Forward();
        }

        private void Back_bttn_Click(object sender, EventArgs e)
        {
            if (ChromiumBrowser.CanGoBack)
                ChromiumBrowser.Back();
        }

        private void Reload_bttn_Click(object sender, EventArgs e)
        {
            if (ChromiumBrowser.IsBrowserInitialized)
                ChromiumBrowser.Reload();
        }

        private void Go_bttn_Click(object sender, EventArgs e)
        {
            var address = GetSiteAddress();

            if (!string.IsNullOrWhiteSpace(address))
            {
                var isValidUrl = Common.HttpStatusCodeChecker.IsStatusOK(address);

                if (isValidUrl == false)
                {
                    address = googleStartPage + "/search?q=" + address.Replace(' ', '+');
                }

                OpenUrl(address);
            }
        }

        private void Search_bttn_Click(object sender, EventArgs e)
            => ChromiumBrowser.Load(googleStartPage);

        private void DevTools_bttn_Click(object sender, EventArgs e)
        {
            if (isDevToolVisible)
            {
                this.browserPage_spltCntnr.SplitterDistance = this.Size.Width;
                isDevToolVisible = false;
                this.browserPage_spltCntnr.Panel2.Hide();
            }
            else
            {
                this.browserPage_spltCntnr.SplitterDistance = this.Size.Width - Visual.Sizes.DevToolsWidth;
                this.browserPage_spltCntnr.Panel2.Show();

                ShowDevTools();
            }
        }

        public IWindowInfo GetWindowInfo()
        {
            var windowInfo = new WindowInfo();
            windowInfo.Width = this.browserPage_spltCntnr.Panel2.Size.Width;
            windowInfo.Height = this.browserPage_spltCntnr.Panel2.Size.Height;
            windowInfo.SetAsChild(browserPage_spltCntnr.Panel2.Handle);

            return windowInfo;
        }

        private void ShowDevTools()
        {
            try
            {
                if (ChromiumBrowser.IsBrowserInitialized)
                {
                    using (var windowInfo = GetWindowInfo())
                    {
                        ChromiumBrowser.ShowDevTools(windowInfo);
                    }
                    isDevToolVisible = true;
                }
            }
            catch(Exception ex)
            {
                Logger.Log.Error("New unhandled exception",ex);
            }
        }

        private void SetStyle()
        {
            navBarContainer.BackColor = Visual.Colors.Background;
            browserTabContainer.BackColor = Visual.Colors.Background;
            browserPage_spltCntnr.BackColor = Visual.Colors.Background;
            webPageContainer_pnl.BackColor = Visual.Colors.Background;
            container_pnl.BackColor = Visual.Colors.Background;
            addressBar_pnl.BackColor = Visual.Colors.Background;

            devTools_button.BackColor = Visual.Colors.ButtonSecondary;
            search_bttn.BackColor = Visual.Colors.ButtonSecondary;
            reload_bttn.BackColor = Visual.Colors.ButtonSecondary;
            forward_bttn.BackColor = Visual.Colors.ButtonSecondary;
            back_bttn.BackColor = Visual.Colors.ButtonSecondary;
            go_bttn.BackColor = Visual.Colors.ButtonAccent;

            addresField_txtBx.BackColor = Visual.Colors.InputField;
        }

        private void AddresField_txtBx_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.Enter: Go_bttn_Click(sender, e); break;
                default: break;
            }
        }

        private void WSBrowser_KeyDown(object sender, KeyEventArgs e)
        {
            
            switch (e.KeyCode)
            { 
                case Keys.F5: this.Refresh(); break;
                case Keys.F12: this.ShowDevTools(); break;
                
                default: break;
            }
        }
    }
}
