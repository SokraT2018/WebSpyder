﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Windows.Forms;

namespace WebSpyder.Controls.RendererControls
{
    public partial class HtmlRenderer : UserControl
    {
        public static ChromiumWebBrowser ChromiumBrowser { get; private set; }
     
        public HtmlRenderer()
        {
            InitializeComponent();
            SetStyle();
            InitializeChromium();
        }

        private void InitializeChromium()
        {
            //Create a ChromiumWebBrowser component
            ChromiumBrowser = new ChromiumWebBrowser(String.Empty)
            {
                BrowserSettings = new BrowserSettings()
                {
                    DefaultEncoding = "utf-8"
                }
            };

            //Add the ChromiumWebBrowser to the form
            this.Controls.Add(ChromiumBrowser);
        }
        
        public static void Render(string html)
        {
            ChromiumBrowser.LoadHtml(html, true);
        }

        public static void SetEncoding(string encoding)
        {
            ChromiumBrowser.BrowserSettings.DefaultEncoding = encoding;
        }

        private void SetStyle()
        {
            this.BackColor = Visual.Colors.MainArea;
        }

    }
}
