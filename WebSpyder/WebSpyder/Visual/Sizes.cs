﻿
namespace WebSpyder.Visual
{
    public static class Sizes
    {
        public static readonly int TopMargin = 50;
        public static readonly int NavigationPanelHeight = 30;

        public static readonly int ParserControlWidth = 400;
        public static readonly int DevToolsWidth = 600;
    }
}


 
    
    
