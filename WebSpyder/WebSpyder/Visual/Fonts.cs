﻿using System.Drawing;

namespace WebSpyder.Visual
{
    public static class Fonts
    {
        public static readonly string MainFont = "Tahoma";

        public static Font GetFont(string fontFamily, System.Single fontSize, FontStyle fontStyle, GraphicsUnit units)
          => new Font(fontFamily, fontSize, fontStyle, units, 0);
    }
}


 
    
    
