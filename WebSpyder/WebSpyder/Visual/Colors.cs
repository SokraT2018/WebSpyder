﻿using System.Drawing;

namespace WebSpyder.Visual
{
    public static class Colors
    {
        public static readonly Color White = Color.FromArgb(255, 255, 255);

        public static readonly Color Background = Color.FromArgb(40, 60, 143);

        public static readonly Color ButtonAccent = Color.FromArgb(38, 198, 218);

        public static readonly Color ButtonSecondary = Color.FromArgb(53, 145, 208);

        public static readonly Color InputField = Color.FromArgb(228, 234, 234);

        public static readonly Color MainArea = InputField;
    }
}


 
    
    
