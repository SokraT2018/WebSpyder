﻿using System;
using System.Windows.Forms;
using WebSpyder.Forms;
using static WebSpyder.Logger;

namespace WebSpyder
{
    public static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        /// 
        private static MainWindow WebSpyderWindow { get; set; }

        [STAThread]
        static void Main()
        {
            Logger.InitializeLogger();

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                WebSpyderWindow = new MainWindow();
                
                Application.Run(WebSpyderWindow);
            }
            catch(InvalidOperationException ex)
            { 
              Log.Error("Invalid Operation Exception", ex);
            }
            catch(Exception ex)
            { 
               Log.Error("New unhandled exception", ex);
            }
        }

    }
}
