﻿
namespace WebSpyder.Forms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.browserTab2 = new System.Windows.Forms.TabPage();
            this.parserTab1 = new System.Windows.Forms.TabPage();
            this.tabCntrl = new System.Windows.Forms.TabControl();
            this.pin = new System.Windows.Forms.PictureBox();
            this.tabCntrl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pin)).BeginInit();
            this.SuspendLayout();
            // 
            // browserTab2
            // 
            resources.ApplyResources(this.browserTab2, "browserTab2");
            this.browserTab2.AllowDrop = true;
            this.browserTab2.Name = "browserTab2";
            // 
            // parserTab1
            // 
            resources.ApplyResources(this.parserTab1, "parserTab1");
            this.parserTab1.Name = "parserTab1";
            // 
            // tabCntrl
            // 
            resources.ApplyResources(this.tabCntrl, "tabCntrl");
            this.tabCntrl.AllowDrop = true;
            this.tabCntrl.Controls.Add(this.parserTab1);
            this.tabCntrl.Controls.Add(this.browserTab2);
            this.tabCntrl.Name = "tabCntrl";
            this.tabCntrl.SelectedIndex = 0;
            // 
            // pin
            // 
            resources.ApplyResources(this.pin, "pin");
            this.pin.Image = global::WebSpyder.Properties.Resources.unpinned;
            this.pin.Name = "pin";
            this.pin.TabStop = false;
            this.pin.Click += new System.EventHandler(this.PinBttn_Click);
            // 
            // MainWindow
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(126)))));
            this.Controls.Add(this.pin);
            this.Controls.Add(this.tabCntrl);
            this.Name = "MainWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Resize += new System.EventHandler(this.MainWindow_SizeChanged);
            this.tabCntrl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCntrl;
        private System.Windows.Forms.TabPage parserTab1;
        private System.Windows.Forms.TabPage browserTab2;
        private System.Windows.Forms.PictureBox pin;
    }
}