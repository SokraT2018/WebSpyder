﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Windows.Forms;
using WebSpyder.Controls.Browsers;
using WebSpyder.Controls.ParserControls;

namespace WebSpyder.Forms
{
    public partial class MainWindow : Form
    {
        public static ParserControl ParserControl { get; private set; }

        public static WSBrowser WSBrowser { get; private set; }

        private static CefSettings cefSettings = new CefSettings();

        public MainWindow()
        {
            InitializeComponent();
            InitializeComponents();
            SetStyle();
            ShowStartScreen();
        }

        private void InitializeComponents()
        {
            InitializeCefSettings();
            InitializeRenderer();
            InitializeBrowser();
            SetComponentsSizes();
        }

        private static void InitializeCefSettings()
        {
            Cef.Initialize(cefSettings);
        }

        private void InitializeRenderer()
        {
            ParserControl = new ParserControl();
            this.parserTab1.Controls.Add(ParserControl);
        }

        private void InitializeBrowser()
        {
            WSBrowser = new WSBrowser();
            this.browserTab2.Controls.Add(WSBrowser);

            if (null != ParserControl)
            {
                var browser = WSBrowser.ChromiumBrowser;
                ParserControl.SetBrowser(browser);
            }
        }

        private void MainWindow_SizeChanged(object sender, EventArgs e)
        {
            SetComponentsSizes();
        }

        private void SetComponentsSizes()
        {
            if (null != WSBrowser)
            {
                WSBrowser.Size = new System.Drawing.Size(Size.Width, Size.Height - Visual.Sizes.NavigationPanelHeight - Visual.Sizes.TopMargin);
                WSBrowser.ChromiumBrowser.Size = new System.Drawing.Size(Size.Width, Size.Height - Visual.Sizes.TopMargin);
            }

            if (null != ParserControl)
            {
                ParserControl.Size = new System.Drawing.Size(Size.Width, Size.Height - Visual.Sizes.TopMargin);
            }
        }

        private void SetStyle()
        {
            BackColor = Visual.Colors.Background;
        }

        private void PinBttn_Click(object sender, EventArgs e)
        {
            TopMost = !TopMost;
            pin.Image = TopMost ? Properties.Resources.pinned : Properties.Resources.unpinned;
        }

        private void ShowStartScreen()
        {
            var startScreen = new StartScreen(this);
            startScreen.Show(8000);
        }
    }
}
