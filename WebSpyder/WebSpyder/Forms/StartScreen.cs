﻿using System;
using System.Windows.Forms;

namespace WebSpyder.Forms
{
    public partial class StartScreen : Form
    {
        private readonly Timer timer = new Timer();

        public StartScreen(Form owner)
        {
            InitializeComponent();
            this.Owner = owner;
            this.Size = this.Owner.Size;
            startScreen_pictureBox.BackColor = Visual.Colors.Background;
        }

        public void Show(int displayingTimeMillisecconds)
        {          
            this.Show();
            timer.Interval = displayingTimeMillisecconds;
            timer.Tick += Close;
            timer.Start();
        }

        private void StartScreen_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void StartScreen_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Close(object sender, EventArgs e)
        {
            timer.Dispose();
            this.Dispose();
        }
    }
}
